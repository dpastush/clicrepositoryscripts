import datetime
import tfs
import io
import glob
import numpy as np
#import matplotlib.pyplot as plt
import jinja2 as jj
import pandas as pnd
#import httpimport 
from webtools import webtools

#with httpimport.remote_repo(['webtools'], 'https://gitlab.cern.ch/acc-models/acc-models-ps/-/raw/2021/_scripts/web/'):
#    from webtools import webtools

pnd.set_option('display.max_rows', None)
pnd.set_option('display.max_columns', None)


# Create SURVEY content
twissFile = "./bds.twiss"
surveyFile = "./survey.txt"

twiss = tfs.read(twissFile)
survey = tfs.read(surveyFile)

#twiss.to_pickle("./bds.pkl")

#parsing the template

jj_env = jj.Environment(loader = jj.FileSystemLoader( searchpath = "." ))

template = jj_env.get_template("index.template")


with open("index.md", 'w') as f:
    f.write(template.render(twiss_data = twiss.to_string(index=False), survey_data = survey.to_string(index=False)))


webtools.create_optics_plots(twiss, './Plot.html')
webtools.create_survey_plot(survey, './surveyPlot.html')
