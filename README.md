# Scripts for CLIC Repository

It is a repository with all files that you need to generate plots and `index.md` for `acc-models-clic`. 

To generate index.md you need to have files: `webtools.py`, `create_web.py`, `index.template`, `job.madx`(without any inserted files),`twiss file` and `survey file`. 

## Libraries

To generate plots I used `Bokeh`. It is a Python library for creating interactive visualizations for modern web browsers. To install this library you need to use this command:

`pip install bokeh` 

### webtools.py
It is a class which contains all functions that you need to create plots. 

### index.template
It is a template for future `index.md`. This file is written in markdonw using MkDocs framework.

### create_web.py
To generate plots and index.md you need to run `create_web.py` by command 

`python3 create_web.py`

After that you will get three more files: `Plot.html`, `surveyPlot.html` and `index.md`. 

`Plot.html` - it is a file which contains twiss plot. To generate this plot, twiss file with all data is called in function `create_optic_plot`.

`surveyPlot.html` - it is a file which containes survey plot. To generate this plot, survey file with all data is called in function `create_survey_plot`.

To get twiss file as pickled DataFrame you need to uncomment the command `twiss.to_pickle`.

`index.md` - it is a markdown file which represent your web page. To generate this file, `index.template` is used that is parsed using `jinjia2`. 