from bokeh.plotting import figure, show, ColumnDataSource, output_file
from bokeh.layouts import column, gridplot
from bokeh.models import Legend, LinearAxis, Range1d, Span
import bokeh.palettes
from numpy import save
import tfs
import os
import numpy as np
import pandas as pnd
import glob



class webtools:

    def create_optics_plots(twiss, output):

        tooltips = [ ("value [m]", "$y")]
        tooltips_elements = [("element", "@NAME")]
        
        plot = figure(title=None, width=1000, height=500, x_axis_label='S[m]', y_axis_label='βx,y[km]',x_range=(0, 1750), y_range=(0, 350), tools='box_zoom, pan, reset, hover, undo, zoom_in, zoom_out', active_drag = 'box_zoom', tooltips = tooltips)
        plot.outline_line_color = 'black'
        plot.sizing_mode = 'scale_width'


        plot.line(list(twiss['S']), list(map(lambda x: x/1000.0, twiss['BETX'])), color="blue", line_width=1.5)

        plot.line(list(twiss['S']), list(map(lambda x: x/1000.0, twiss['BETY'])), color="red", line_width=1.5)

        plot.extra_y_ranges = {"Dx": Range1d(start=-0.6, end=0.6)}

        plot.add_layout(LinearAxis(y_range_name = "Dx", axis_label='Dx [m]'), 'right')

        plot.line(list(twiss['S']), list(twiss['DX']), color="black", y_range_name="Dx", line_width=1.5)

        legend = Legend(items=[(u"\u03B2x", [plot.renderers[0]]), (u"\u03B2y", [plot.renderers[1]]), ("Dx", [plot.renderers[2]])], location=(40, -100))
        #plot.add_layout(legend, 'left')


        plot2 = figure(title=None, width=1000, height=100, x_range=plot.x_range, y_range=(-1.25,1.25),  tools='box_zoom, reset, hover, undo, zoom_in, zoom_out', active_drag = 'box_zoom', tooltips = tooltips_elements)
        plot2.axis.visible = False
        plot2.grid.visible = False
        plot2.outline_line_color = None
        plot2.sizing_mode = 'scale_width'
        plot2.toolbar.logo = None
        plot2.toolbar_location = None

        webtools.plot_lattice_elements(plot2, twiss)

        output_file(output)
        show(column([plot2, plot]), sizing_mode = 'scale_width')

    def create_survey_plot(survey, output):

        tooltips_elements = [("element", "@NAME")]

        plot = figure(title=None, width=1000, height=300, x_axis_label='Z[m]', y_axis_label='X[m]', x_range=(0, 1750), y_range=(-2, 1),  tools='box_zoom, pan, reset, hover, undo, zoom_in, zoom_out', active_drag = 'box_zoom', tooltips = tooltips_elements)
        plot.outline_line_color = 'black'

        plot.line(list(survey['Z']), list(survey['X']), color="black", line_width=1.5)

        survey['POSITION']=survey['Z'] - 0.5*survey['L']

        src_q = ColumnDataSource(survey.loc[ survey.KEYWORD.str.contains('QUADRUPOLE')])
        plot.rect(x='POSITION',
                        y='X',
                        width='L',
                        height=0.6,
                        fill_color=bokeh.palettes.Reds8[2],
                        line_color = 'black',
                        source=src_q)

        src_m = ColumnDataSource(survey.loc[ survey.KEYWORD.str.contains('MONITOR')])
        plot.rect(x='POSITION',
                        y='X',
                        width='L',
                        height = 1, 
                        fill_color='gray', 
                        line_color = 'black',
                        source=src_m)

        src_s = ColumnDataSource(survey.loc[ survey.KEYWORD.str.contains('SBEND')])
        plot.rect(x='POSITION',
                        y='X',
                        width='L',
                        height=0.5,
                        fill_color=bokeh.palettes.Blues8[1],
                        line_color='black',
                        source=src_s)

        src_r = ColumnDataSource(survey.loc[ survey.KEYWORD.str.contains('RBEND')])
        plot.rect(x='POSITION',
                        y='X',
                        width='L',
                        height=0.5,
                        fill_color=bokeh.palettes.Blues8[1],
                        line_color='black',
                        source=src_r)

        src_sexut = ColumnDataSource(survey.loc[ survey.KEYWORD.str.contains('SEXTUPOLE')])
        plot.rect(x='POSITION',
                        y='X',
                        width='L',
                        height = 0.4, 
                        fill_color='#fff7bc', 
                        line_color = 'black',
                        source=src_sexut)


        src_oct = ColumnDataSource(survey.loc[ survey.KEYWORD.str.contains('OCTUPOLE')])
        plot.rect(x='POSITION',
                        y='X',
                        width='L',
                        height = 0.3, 
                        fill_color='#756bb1', 
                        line_color = 'black',
                        source=src_oct)

        src_k = ColumnDataSource(survey.loc[ survey.KEYWORD.str.contains('KICKER')])
        plot.rect(x='POSITION',
                        y='X',
                        width='L',
                        height = 1, 
                        fill_color='#1b9e77', 
                        line_color = 'black',
                        source=src_k)


        output_file(output)
        show(plot)

    def plot_lattice_elements(figure, twiss):
            
        twiss['POSITION']=twiss['S'] - 0.5*twiss['L']

        src_q = ColumnDataSource(twiss.loc[ twiss.KEYWORD.str.contains('QUADRUPOLE')])
        figure.rect(x='POSITION',
                        y=0,
                        width='L',
                        height=1.2,
                        fill_color=bokeh.palettes.Reds8[2],
                        line_color = 'black',
                        source=src_q)

        src_m = ColumnDataSource(twiss.loc[ twiss.KEYWORD.str.contains('MONITOR')])
        figure.rect(x='POSITION',
                        y=0,
                        width='L',
                        height = 2, 
                        fill_color='gray', 
                        line_color = 'black',
                        source=src_m)

        src_s = ColumnDataSource(twiss.loc[ twiss.KEYWORD.str.contains('SBEND')])
        figure.rect(x='POSITION',
                        y=0,
                        width='L',
                        height=1,
                        fill_color=bokeh.palettes.Blues8[1],
                        line_color='black',
                        source=src_s)

        src_r = ColumnDataSource(twiss.loc[ twiss.KEYWORD.str.contains('RBEND')])
        figure.rect(x='POSITION',
                        y=0,
                        width='L',
                        height=1,
                        fill_color=bokeh.palettes.Blues8[1],
                        line_color='black',
                        source=src_r)

        src_sexut = ColumnDataSource(twiss.loc[ twiss.KEYWORD.str.contains('SEXTUPOLE')])
        figure.rect(x='POSITION',
                        y=0,
                        width='L',
                        height = 0.8, 
                        fill_color='#fff7bc', 
                        line_color = 'black',
                        source=src_sexut)


        src_oct = ColumnDataSource(twiss.loc[ twiss.KEYWORD.str.contains('OCTUPOLE')])
        figure.rect(x='POSITION',
                        y=0,
                        width='L',
                        height = 0.6, 
                        fill_color='#756bb1', 
                        line_color = 'black',
                        source=src_oct)

        src_k = ColumnDataSource(twiss.loc[ twiss.KEYWORD.str.contains('KICKER')])
        figure.rect(x='POSITION',
                        y=0,
                        width='L',
                        height = 2, 
                        fill_color='#1b9e77', 
                        line_color = 'black',
                        source=src_k)

        hor_line = Span(location=0, dimension='width', line_color='black', line_width=0.5)
        figure.add_layout(hor_line)

